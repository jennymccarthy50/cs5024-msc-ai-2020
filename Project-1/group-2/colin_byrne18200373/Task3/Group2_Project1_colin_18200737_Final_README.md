## Project 1 - Chain Reaction - Final Model

5. Reflect - How can you improve your own strategy by including ideas and artefacts from your peers? Do this by reflecting, examining, and evaluating the models and strategies shared by your peers 
6. Task 3 - Improve your strategy - build the most effective strategy 
7. Submit - Upload your revised new strategy as a new strategy (worth 10%) (due Mar 11th)
8. Reflect - On the design process and outcome, for example, modeling techniques used and the approach that you took to design and improve your AI 

**Student Name:** Colin Byrne

**Student Id:** 18200737

**Strategy:**  
Review student Ai models. 
Select the best two.
Pit them against the Canned AI, Demo, Standard etc
Score the two student Ai and the available demoAi in terms of 
performance, under Attack and defend heuristic. 
Reverse Engineer all three AI and try to refine the best heuristic from these models.
Take corners 1st
Check if Critical
Check if indanger
Main idea to look ahead and determine before and after atom placement score.


 * Project1_Task3_Colin_18200737_MainModelA- High level process
* Subprocess file.
   * checkdanger. 
   * checklocation.
   * checknextmove.
 
** Note Ive included Project1_Task3_Colin_18200737_MainModelA_rev1 as this file calls checknextmove subprocess but it hangs after approx 20 moves. All files pass validation and I'm unable to debug the source of the hang.
     