# Project-1

For this "improved" strategy is based on Brilliant.org's [Chain Reaction Heuristics](https://brilliant.org/wiki/chain-reaction-game/).
I tried to implement steps 3--6 in my strategy; 1 and 2 seemed irrelevant and I couldn't figure out how to do 7.

## Note

Unfortunately, my AI doesn't run in the CrGame or the Tournament. I spent a
long long time trying to figure out why but with no debug info to go on, I
gave up.

