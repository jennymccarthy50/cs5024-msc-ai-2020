# Project 1 - Chain Reaction

Name: Gerard Murphy 
Email: 18204937@studentmail.ul.ie

## Task 2 - Initial AI

### Strategy - SecondAI
* Prioritize our cells that are endangered
* If not endangered, avoid exploding critical cells
* Choose Corners over edges
* Choose Edges over Middle

## Task 3 - Final AI

### Strategy - Group2_Gerard_Murphy_FinalAI
* Start with 200 points
* Check outcome if atom is placed on field
  * Add point for each cell owned after move
  * Deduct point for each field endangered for player after move
* Prioritize our cells that are endangered - plus 100 points
* If not endangered, avoid exploding critical cells - deduct 100 points
* Choose Corners over edges - plus 40 points
* Choose Edges over Middle - plus 10 points
