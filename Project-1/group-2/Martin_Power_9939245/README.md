## Project 1 - Chain Reaction
Name: Martin Power
Email: 9939245@studentmail.ul.ie

## Task 3 - Final AI

Strategy - FinalAI_Process_MartinPower_9939245 (issues with file renaming in DIME)

This strategy does not try to make any brute force decision based on where a cell is located (edge/middle/corner). Instead, the strategy tries to big general goals based on the game and optimize these.

The primary goal is reduce the number of atoms your opponent has to zero and this is given the most weighting in the strategy.

All parts of the strategy have "scale" values that can be used to adjust the weighting of each goals.

The goals in order of priority are:
* Minimize the number of atoms your opponent has (getting this to zero wins the game)
* Maximizing the number of endangered cells your opponent has
* Maximizing the number of critical cells I have (this will cause the AI to choose corners over edges, edgers over middle)
* Minimizing the number of endangered cells I have
* Minimizing the number of critical cell the opponent has (this gives them less room to manouevre)

This strategy beats the StandardAI about 45% of the time and the DemoAI about 75% of the time
