## Project 1 - Chain Reaction - Final Model

5. Reflect - How can you improve your own strategy by including ideas and artefacts from your peers? Do this by reflecting, examining, and evaluating the models and strategies shared by your peers 
6. Task 3 - Improve your strategy - build the most effective strategy 
7. Submit - Upload your revised new strategy as a new strategy (worth 10%) (due Mar 11th)
8. Reflect - On the design process and outcome, for example, modeling techniques used and the approach that you took to design and improve your AI 

**Student Name:** Gerry Carty

**Student Id:** 18217249

**Strategy:**  Patience is more Rewarding.  
First simulate a placement on each valid cell.  Based on this, determine the difference in atom and cell count my AI player would have before and after each possible placement.
Add these 2 numbers together for each placement.
Then determine a weight based on the danger each cell poses or is in based on cell location and it's endangered and/or critical state.
Add this weight to the atom/cell score.
The cell with the highest score is selected.  
Definitely an improvement on my first model.

 * Group2-Gerry-Carty-FinalAI - High level process
   * MostAtom.process - Count the number of atoms AI player would have after next placement.
   * MostCell.process - Count the number of cells AI player would have after next placement.
   * WeightDanger.process - Weight cell based on location and whether it's critical or endangered.
     *  GC_Corner_Check.process - Is cell a corner, edge or middle cell.
     *  IsCritOrEndgr.process - Is cell critical, endangered, critical and endangered or not critical or endangered
	 
Zip file Group2-Gerry-Carty-FinalAI.zip contains all the above also.