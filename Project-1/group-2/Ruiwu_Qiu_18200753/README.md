# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Ruiwu Qiu

**Student ID:** 18200753

**Student Email:** 18200753@studentmail.ul.ie

---
Deliverables:

| Filename                                        | Description                                                                                                                 |
|-------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------|
| RickAI.process                                  | A top level Chain Reaction AI (Main) model (created in Task 2, used in Task 2 only)                                         |
| IsCornerOrEdge.process                          | A sub AI provides test on if a cell is a corner or edge cell (created in Task 2, used in both Task 2 & Task 3)              |
| Group2_RuiwuQiu_IsEndangedAndCritical.process   | A sub AI provides test on if a cell owned by the current player is both in danger and critical condition (created in Task 3)|
| Group2_RuiwuQiu__OneStepLookAhead.process       | A sub AI evaluates how good a cell is by a field simulation of 1-step lookahead (created in Task 3)                         |
| Group2_RuiwuQiu_FinalAI.process                 | A top level Chain Reaction AI (Main) model (created in Task 3)                                                              |          