# Project 1

**Name:** Ken Power <br/>
**ID**18200729 <br/>



## Task 3: Now improve your CR AI

### Instructions

1) Upload your revised strategy as a new strategy to the Gitlab  (named ```<GroupN>-<Name-Surname>-FinalAI```), again in your directory.

2) Please submit the link to the Gitlab in time to the Assignment page, using the link below. (Note that the link will not become active until the assignment opens on Mar 9th.)

Pay attention NOT to OVERWRITE your initial AI!

No need this time to post anything to a forum.

### Response

The Task 3 response, with model details, [can be found here.](models/task3/README.md)


## Task 2: Build your complete Chain Reaction AI

### Instructions
Your next task is to build a Chain Reaction AI using the DSL provided in DIME.

In DIME, these are main steps you will likely need: 

1. Add your First SIB 
2. Add a new data context 
3. Add a corner Check 
4. Calculate the final score of your game 
5. Play against your own AI – Can you beat it? 
6. Group a feature of the AI into a reusable SIB (Hint – use Hierarchy for the Sub-AI Feature) 
7. Ensure the neatness and readability of the model (Hint – appropriate use of Hierarchy and layout) 

### Response

**Strategy**
* Determine if a cell is a corner, edge, or middle cell
* For the purpose of this exercise, I am considering corner cells to be the most valuable, followed by edge cells.
* Therefore, add 100 to the cell score for corner cells. Add 25 to the cell score for edge cells.

**Some heuristics for the strategy:**
* First move is always capture a corner, no matter which player goes first. Take as many corners as possible in the opening moves.
* Always capture the corners when possible. 
* Split to recapture corners.
* Bring edge cells to critical.
* Don't explode your own cells unnecessarily. 
* Explode your cell if a neighbour enemy cell is critical.
* Have at least one middle or edge cell that is not in contact with the enemy. This gives us a chance to stay in the game if the enemy captures our main block(s) of cells. 
* If the enemy has captured a corner, lure them out by tricking them into capturing one of your adjacent edge cells. Then re-take the corner.

**SIB Model files**

The models are contained in the [models subdirectory](models/)
* [Main AI Process](models/Project1Task2AI.process)
* [Subprocess to determine if a cell is a corner or an edge](models/IsCornerOrEdgeCell.process)
* [Subprocess to add points to the score if the cell is a corner or an edge](models/AddScoresToCornerOrEdgeCells.process)
* [Process that descrobes a basic game play strategy](models/BasicStrategy.process)
* [Subprocess that attempts to capture an edge cell](models/TryToCaptureAnEdgeCell.process)
* [Subprocess that attempts to capture a middle cell](models/TryToCaptureMiddleCell.process)

**Screenshots**

Main AI Process:
![Main AI Process](images/Screenshot.KensChainReactionAI_models_Project1Task2AI_process.jpg)

Subprocess to determine if a cell is a corner or an edge:
![Subprocess to determine if a cell is a corner or an edge](images/Screenshot.KensChainReactionAI_models_IsCornerOrEdgeCell_process.jpg)

Subprocess to add points to the score if the cell is a corner or an edge:
![Subprocess to add points to the score if the cell is a corner or an edge](images/Screenshot.KensChainReactionAI_models_AddScoresToCornerOrEdgeCells_process.jpg)

