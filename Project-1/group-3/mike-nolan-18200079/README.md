# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Michael Nolan

**Student ID:** 18200079

**Student Email:** 18200079@studentmail.ul.ie

---

## Task 2

Please see inside the `Task2` folder for info and files related to that task.

## Task 3

Please see inside the `Task3` folder for info and files related to that task.
