# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Michael Nolan

**Student ID:** 18200079

**Student Email:** 18200079@studentmail.ul.ie

---

## Task 3

The files in the following table are related to Task 3 and therefore the final submission for Project 1. Each sub-process returns a calculated score that is then added to the final score.

- `Group3_Michael_Nolan_FinalAI.process` is the main AI process and should be the one that is used in game.

- `Group3_Michael_Nolan_CalculateScoreAI.process` is a sub-process that is used in the main process above.

- `MNScoreAtomsCellsAI.process` is also a sub-process that is used in the Calculate Score AI.

- `MNScoreCriticalEndangeredAI.process` is also a sub-process that is used in the Calculate Score AI.

- `MNScoreEdgeCornerAI.process` is also a sub-process that is used in the Calculate Score AI.

| _Filename_                                      | _Description_                                          |
|-------------------------------------------------|--------------------------------------------------------|
| `Group3_Michael_Nolan_FinalAI.process`          | Main Process                                           |
| `Group3_Michael_Nolan_CalculateScoreAI.process` | Sub Process for score calculation                      |
| `MNScoreAtomsCellsAI.process`                   | Sub Process for scoring atoms and cells                |
| `MNScoreCriticalEndangeredAI.process`           | Sub Process for scoring critical and endangered fields |
| `MNScoreEdgeCornerAI.process`                   | Sub Process for scoring for scoring edges and corners  |
