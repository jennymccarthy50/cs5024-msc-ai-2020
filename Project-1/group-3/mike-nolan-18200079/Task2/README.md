# CS5024 - Theory & Practice of Advanced AI Ecosystems

## Project 1

**Student Name:** Michael Nolan

**Student ID:** 18200079

**Student Email:** 18200079@studentmail.ul.ie

---

## Task 2

The files in the table below that are marked in bold are used in my Task 2 submission.

- `MNTask2AI.process` is the main AI process and should be the one that is used in game.

- `CalculateScoreAI.process` is a sub-process that is used in the main process above.

- `MNSubAI.process` is from the module materials and is also a sub-process that is used in the Calculate Score AI.

| _Filename_                     | _Description_                                               |
|--------------------------------|-------------------------------------------------------------|
| **`MNTask2AI.process`**        | **Task 2 - Main Process**                                   |
| **`CalculateScoreAI.process`** | **Task 2 - Sub Process for score calculation**              |
| **`MNSubAI.process`**          | **The Sub AI - from module material. Also used in Task 2.** |
| `MN-AI.process`                | Your First AI - from module material                        |
| `MNTopLevelAI.process`         | The Top Level AI - from module material                     |
