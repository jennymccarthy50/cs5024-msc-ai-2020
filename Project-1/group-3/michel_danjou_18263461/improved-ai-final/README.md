# group3-michel-danjou-FinalAI
## Description
**group3-michel-danjou-FinalAI** is an improved version of the **Stragegy** process published last week.

The heuristic can be seen as having 2 layers for calculating the `score`

**Layer 1: calculate a `score` based on the cell type.**

   * Corners are given an initial `score` of 20
   * Edges are given an initial `score` of 10
   * Middle cells:
     * cells that are `critical` and `endangered` receive an initial `score` of  15
     * others receive an initial `score` of 5

**Layer 2: Evaluate the current player's gains and enemy's losses**

This is a look ahead where we establish how much cells will the current player and the enemy have after a move.
The number of cells gained and lost is added to the `score`.     

The formula for the score is as follows:
```
score = score from layer 1 + current player gained cells + enemy lost cells 
```

This heuristic has acheived 33% against DemoAI.

## Files
  * group3-michel-danjou-FinalAI.process
    * This is the main process
  * IsCornerOrEdge.process
    * This is the sub process from week 1
  * maximise_our_gains_and_enemy_losses.process
    * This is the new sub process for calculating the currentl player's gains and the enemy's losses.
    
  