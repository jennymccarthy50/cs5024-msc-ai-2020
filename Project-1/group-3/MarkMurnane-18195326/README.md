# CS5024 - Theory & Practice of Advanced AI Ecosystems - Spring 2020

# Project 1 - Chain Rection

## Student:      Mark Murnane
## Student ID: 18195326

---

## Task 2 - Build your complete Chain Reaction AI


My Task 2 submission is an AI named **HAL100** as it is both Heuristic and Algorithmic.  Like it's more advanced namesake I also expect it to go horribly wrong.

The strategy of HAL100 is to score (in-order of preference) Middle, Edge and Corner cells.  Crucially it favours cell that are reaching critical mass, and in particular critical cells that are in danger of being taken over by the opposing player.  In tests this has made from some interesting games.

### Future Enhancements

As a kind of pre-reflection exercise, there are two aspects I intend to look into.  

 - On the non-critical branch it might be useful to incorporate an element of randomness to boost scores.  This is to introduce some level of unexpected behaviour in to the play, and also provide a hedge.  I've noted in games that sometimes an unlikely Edge or Middle cell can be almost surrounded and yet be a winning move. 

  - On the critical branch, to simulate a play and measure the impact of exploding this particular cell.
  

### Submission Files

My submission consists of 2 process files:

 - `HAL100.process` contains the main AI strategy and uses basic SIBS, SIBs from `cr.sib` and the _IsCornerOrEdge_ sub-SIB from below.

 - `IsCornerOrEdge.process` implements a sub-SIB to check if a particular cell is a Corner, Edge or Middle cell.

