# Project 1

## Task 1

* [GillesAI_v1.process](GillesAI_v1.process)

## Task 2

* [GillesAI.process](GillesAI_v1.process)
* [IsEdgeOrCorner.process](GillesAI_v1.process)

## Task 3

* [GillesAI_Task3.process](GillesAI_Task3.process)
* [GillesAI_Task3_CellWeight.process](GillesAI_Task3_CellWeight.process)

## Notes

Submitting current work in files in gitlab.

However, development was plagued by runtime issues (add to open and close software repeatedly, delete old jars file, delete old .metadata/ folder, tournament mode does not seem to work anymore..)
